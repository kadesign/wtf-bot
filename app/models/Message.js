const util = require('util');
const { getFullKeyPath } = require('@kadesign/toolbox-js');

const MessageTemplates = require('../components/messageTemplates');

/**
 * Message entity with its text and message code
 *
 * @class
 *
 * @category Models
 */
class Message {
  /**
   * Creates Message instance
   *
   * @param {string}    text                     Message template
   * @param {?string[]} [replacements]           Replacements for placeholders in template
   * @param {boolean}   [markdown=false]         True if message is formatted with Markdown
   * @param {number}    [timeoutBeforeNextMsg=0] Delay before next message in message sequence (in milliseconds)
   * @param {boolean}   [isSilent=false]         True if message should be sent with disabled notifications
   */
  constructor(text, replacements = [], markdown = false, timeoutBeforeNextMsg = 0, isSilent = false) {
    /**
     * Message template
     *
     * @type {string}
     *
     * @private
     */
    this._text = text;

    /**
     * Replacements for placeholders in template
     *
     * @type {string[]}
     *
     * @private
     */
    this._replacements = replacements || [];

    /**
     * True if message is formatted with Markdown
     *
     * @type {boolean}
     *
     * @private
     */
    this._markdown = markdown;

    /**
     * Delay before next message in message sequence (in milliseconds)
     *
     * @type {number}
     *
     * @private
     */
    this._timeout = timeoutBeforeNextMsg;

    this._isSilent = isSilent;
  }

  /**
   * Formatted message text
   *
   * @returns {string}
   */
  get text() {
    return this._replacements.length > 0 ? util.format(this._text, ...this._replacements) : this._text;
  }

  /**
   * Message code
   *
   * @returns {string}
   */
  get code() {
    return getFullKeyPath(MessageTemplates, this._text) || 'unknown';
  }

  /**
   * True if message is formatted with Markdown
   *
   * @returns {boolean}
   */
  get markdown() {
    return this._markdown;
  }

  /**
   * Delay before next message in message sequence (in milliseconds)
   *
   * @returns {number}
   */
  get timeout() {
    return this._timeout;
  }

  /**
   * True if message should be sent with disabled notifications
   *
   * @returns {boolean}
   */
  get isSilent() {
    return this._isSilent;
  }
}

module.exports = Message;