const MessageTemplates = require('../components/messageTemplates');
const Message = require('./Message');

/**
 * Error message instance
 *
 * @class
 * @extends Message
 *
 * @category Models
 */
class ErrorMessage extends Message {
  /**
   * Create error message
   *
   * @param {string} [message] Message text
   */
  constructor(message) {
    super((message ? message : MessageTemplates.errors.generic), null, true);
  }
}

module.exports = ErrorMessage;