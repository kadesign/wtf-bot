/**
 * Message sequence holder
 *
 * @class
 *
 * @category Models
 */
class MessageSequence {
  /**
   * Creates MessageSequence instance
   *
   * @param {Message[]} sequence
   */
  constructor(sequence) {
    /**
     * Message sequence array
     *
     * @type {Message[]}
     *
     * @public
     */
    this.sequence = sequence;
  }
}

module.exports = MessageSequence;