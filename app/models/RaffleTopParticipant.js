/**
 * Participant entry from top rankings
 *
 * @class
 *
 * @category Models
 */
class RaffleTopParticipant {
  /**
   * Creates RaffleTopParticipant instance
   *
   * @param {number}  id       Participant ID
   * @param {?string} username Participant's username
   * @param {number}  total    Total count of wins in period
   */
  constructor(id, username, total) {
    /**
     * Participant ID
     *
     * @type {number}
     *
     * @public
     */
    this.id = id;

    /**
     * Participant's username
     *
     * @type {?string}
     *
     * @public
     */
    this.username = username;

    /**
     * Total count of wins in period
     *
     * @type {number}
     *
     * @public
     */
    this.total = total;
  }
}

module.exports = RaffleTopParticipant;