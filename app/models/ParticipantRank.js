/**
 * Rank of participant
 *
 * @class
 *
 * @category Models
 */
class ParticipantRank {
  /**
   * Creates ParticipantRank instance
   *
   * @param {number}  id       Participant ID
   * @param {number}  year     Year of rank
   * @param {boolean} isWinner True if participant is the winner of the year
   */
  constructor(id, year, isWinner) {
    /**
     * Participant ID
     *
     * @type {number}
     *
     * @public
     */
    this.id = id;

    /**
     * Year of rank
     *
     * @type {number}
     *
     * @public
     */
    this.year = year;

    /**
     * True if participant is the winner of the year
     *
     * @type {boolean}
     *
     * @public
     */
    this.isWinner = isWinner;
  }
}

module.exports = ParticipantRank;