const { DateTime } = require('luxon');

/**
 * Chat information
 *
 * @class
 *
 * @category Models
 */
class Chat {
  /**
   * Create Chat instance
   *
   * @param {number} id                       Chat ID
   * @param {number} tgId                     Chat's Telegram ID
   * @param {number} [lastRaffleWinnerId]     ID of last winner of regular raffle
   * @param {Date}   [lastRaffleDate]         Date of last successful raffle run
   * @param {number} [lastGotyRaffleWinnerId] ID of last winner of GOTY raffle
   * @param {Date}   [lastGotyRaffleDate]     Date of last GOTY raffle
   */
  constructor(id, tgId, lastRaffleWinnerId, lastRaffleDate, lastGotyRaffleWinnerId, lastGotyRaffleDate) {
    /**
     * Chat ID
     *
     * @type {number}
     *
     * @public
     */
    this.id = id;

    /**
     * Chat's Telegram ID
     *
     * @type {number}
     *
     * @public
     */
    this.tgId = tgId;

    /**
     * ID of last winner of regular raffle
     *
     * @type {?number}
     *
     * @public
     */
    this.lastRaffleWinnerId = lastRaffleWinnerId || null;

    const timezone = process.env.WTF_RAFFLE_TZ || 'UTC';
    /**
     * Date of last successful raffle run
     *
     * @type {?DateTime}
     *
     * @public
     */
    this.lastRaffleDate = lastRaffleDate ? DateTime.fromJSDate(lastRaffleDate, { zone: timezone }) : null;

    /**
     * ID of last winner of GOTY raffle
     *
     * @type {?number}
     *
     * @public
     */
    this.lastGotyRaffleWinnerId = lastGotyRaffleWinnerId || null;

    /**
     * Date of last GOTY raffle
     *
     * @type {?DateTime}
     *
     * @public
     */
    this.lastGotyRaffleDate = lastGotyRaffleDate ? DateTime.fromJSDate(lastGotyRaffleDate, { zone: timezone }) : null;
  }

  /**
   * Year of last GOTY raffle
   *
   * @type {?number}
   *
   * @public
   */
  get lastGotyRaffleYear() {
    return this.lastGotyRaffleDate?.year|| null;
  }
}

module.exports = Chat;