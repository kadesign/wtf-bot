/**
 * Raffle participant
 *
 * @class
 *
 * @category Models
 */
class Participant {
  /**
   * Creates Participant instance
   *
   * @param {number}  id                Participant ID
   * @param {number}  tgId              Participant's Telegram ID
   * @param {string}  username          Participant's username
   * @param {number}  chatId            Chat ID
   * @param {boolean} isActive          True if participant is active in raffle
   * @param {number}  [currentCombo=0]  Current win streak
   * @param {number}  [maxCombo=0]      Maximal win streak
   * @param {?number} [totalHits]       Total amount of wins
   * @param {?number} [currentYearHits] Amount of wins in current year
   */
  constructor(id, tgId, username, chatId, isActive, currentCombo = 0, maxCombo = 0, totalHits = null, currentYearHits = null) {
    /**
     * Participant ID
     *
     * @type {number}
     *
     * @public
     */
    this.id = id;

    /**
     * Participant's Telegram ID
     *
     * @type {number}
     *
     * @public
     */
    this.tgId = tgId;

    /**
     * Participant's username
     *
     * @type {string}
     *
     * @public
     */
    this.username = username;

    /**
     * Chat ID
     *
     * @type {number}
     *
     * @public
     */
    this.chatId = chatId;

    /**
     * True if participant is active in raffle
     *
     * @type {boolean}
     *
     * @public
     */
    this.isActive = isActive;

    /**
     * Current win streak
     *
     * @type {number}
     *
     * @public
     */
    this.currentCombo = currentCombo;

    /**
     * Maximal win streak
     *
     * @type {number}
     *
     * @public
     */
    this.maxCombo = maxCombo;

    /**
     * Total amount of wins.
     * Null if counter is not relevant for request
     *
     * @type {?number}
     *
     * @public
     */
    this.totalHits = totalHits;

    /**
     * Amount of wins in current year.
     * Null if counter is not relevant for request
     *
     * @type {?number}
     *
     * @public
     */
    this.currentYearHits = currentYearHits;
  }
}

module.exports = Participant;