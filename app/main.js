// External libs
if (process.env.SENTRY_DSN) {
  const Sentry = require('@sentry/node');
  Sentry.init({ environment: process.env.NODE_ENV || undefined });
}

// Internal libs
const MicroLog = require('@kadesign/microlog').default;
const BotManager = require('./components/botManager');

const Logger = new MicroLog();

new BotManager().start().catch(err => Logger.error(err));