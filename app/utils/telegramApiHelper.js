/**
 * @module TelegramApiHelper
 * @desc Helper methods for working with Telegram API (Telegraf.js library)
 *
 * @category Utilities
 */

/**
 * Sends message to Telegram chat
 *
 * @param {Context}  ctx     Telegraf context
 * @param {Message}  message Message to send
 * @param {MicroLog} logger  Logger instance
 *
 * @public
 */
module.exports.sendMessage = (ctx, message, logger) => {
  ctx.telegram.sendMessage(ctx.message.chat.id, message.text, {
    parse_mode: (message.markdown ? "Markdown" : undefined),
    disable_notification: message.isSilent
  })
    .then(() => {
      logger.log(`Message ${message.code} sent to chat ${ctx.message.chat.id}${message.isSilent ? ' in silent mode' : ''}`);
    });
};

/**
 * Send message sequence to Telegram chat
 *
 * @param {Context}         ctx    Telegraf context
 * @param {MessageSequence} msgSeq Message sequence to send
 * @param {MicroLog}        logger Logger instance
 *
 * @public
 */
module.exports.sendMessageSequence = (ctx, msgSeq, logger) => {
  let nextTimeout = 0;

  msgSeq.sequence.forEach(message => {
    setTimeout(() => {
      this.sendMessage(ctx, message, logger);
    }, nextTimeout);

    logger.log(`Message ${message.code} is scheduled to be sent to chat ${ctx.message.chat.id} after ${nextTimeout} ms`);

    nextTimeout += message.timeout;
  });
};