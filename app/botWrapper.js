const { Telegraf } = require('telegraf');
const SocksAgent = require('socks5-https-client/lib/Agent');
const { checkEnvVariables } = require('@kadesign/toolbox-js');

/**
 * Telegraf bot instance wrapper
 *
 * @class
 */
class BotWrapper {
  /**
   * Instantiates Telegraf bot with configuration from environment variables
   */
  constructor() {
    checkEnvVariables('BOT_NAME', 'BOT_TOKEN');

    let botOpts;
    if (process.env.NODE_ENV === 'development' && process.env.DEV_PROXY_HOST && process.env.DEV_PROXY_PORT) {
      const proxyAgentOpts = {
        socksHost: process.env.DEV_PROXY_HOST,
        socksPort: process.env.DEV_PROXY_PORT
      };

      if (process.env.DEV_PROXY_USER) proxyAgentOpts.socksUser = process.env.DEV_PROXY_USER;
      if (process.env.DEV_PROXY_PASSWORD) proxyAgentOpts.socksPassword = process.env.DEV_PROXY_PASSWORD;

      botOpts = {
        username: process.env.BOT_NAME,
        telegram: {
          agent: new SocksAgent(proxyAgentOpts)
        }
      };
    } else {
      botOpts = {
        username: process.env.BOT_NAME
      };
    }

    /**
     * Telegraf bot instance
     *
     * @type {Telegraf}
     *
     * @public
     */
    this.bot = new Telegraf(process.env.BOT_TOKEN, botOpts);
  }
}

module.exports = BotWrapper;