const MicroLog = require('@kadesign/microlog').default;

const BotWrapper = require('../botWrapper');
const BotCommands = require('./botCommands');
const DbManager = require('./database/dbManager');
const CommandsHandler = require('./commandsHandler');
const ListenersManager = require('./listenersManager');
const ShutdownEventHandler = require('./shutdownEventHandler');

/**
 * Bot manager.
 * Main entry point for the application
 *
 * @class
 * @hideconstructor
 *
 * @category Components
 */
class BotManager {
  /**
   * Logger instance
   *
   * @type {MicroLog}
   *
   * @private
   */
  _logger = new MicroLog(this);

  /**
   * Wrapper for Telegraf bot with its settings
   *
   * @type {BotWrapper}
   *
   * @private
   */
  _botWrapper;

  /**
   * Reference to Telegraf bot instance
   *
   * @type {Telegraf}
   *
   * @private
   */
  _telegrafBot;

  /**
   * DbManager instance
   *
   * @type {DbManager}
   *
   * @private
   */
  _dbMgr;

  /**
   * CommandsHandler instance
   *
   * @type {CommandsHandler}
   *
   * @private
   */
  _commandsHandler;

  /**
   * ListenersManager instance
   *
   * @type {ListenersManager}
   *
   * @private
   */
  _listenersMgr;

  /**
   * ShutdownEventHandler instance
   *
   * @type {ShutdownEventHandler}
   *
   * @private
   */
  _shutdownEventHandler;

  /**
   * Starts the bot
   *
   * @return {Promise<void>}
   *
   * @public
   * @async
   */
  async start() {
    await this._init();

    await this._telegrafBot.launch({ dropPendingUpdates: true });
  }

  /**
   * Initialize and check all components of the application
   *
   * @return {Promise<void>}
   *
   * @private
   * @async
   */
  async _init() {
    this._botWrapper = new BotWrapper();

    this._logger.log(`Starting ${process.env.BOT_NAME}`);
    this._logger.log(`Current version: ${process.env.npm_package_version}`);
    if (process.env.NODE_ENV) this._logger.log(`Running in ${process.env.NODE_ENV} mode`);

    this._logger.log('Connectivity check started');

    this._telegrafBot = this._botWrapper.bot;
    await this._checkTelegramAPI();
    this._dbMgr = new DbManager();
    await this._checkDb();

    this._logger.log('Connectivity check finished');

    this._logger.log('Configuring middlewares');
    await this._configureMiddlewares();

    this._logger.log('Registering commands');
    this._commandsHandler = new CommandsHandler(this._telegrafBot, this._dbMgr);
    this._commandsHandler.register();
    await this._commandsHandler.updateTelegramCommandList();

    this._logger.log('Registering listeners');
    this._listenersMgr = new ListenersManager(this._telegrafBot, this._commandsHandler._participantManager);
    this._listenersMgr.register();

    this._logger.log('Registering shutdown hook');
    this._shutdownEventHandler = new ShutdownEventHandler();
    this._registerShutdownEventHandlers();

    this._logger.log('Ready.');

    try {
      let chatsCount = await this._dbMgr.getChatsCount();
      this._logger.log(`Chats in-game: ${chatsCount}`);
    } catch (e) {
      throw new Error(`Database access error: ${e.message}`);
    }
  }

  /**
   * Check availability of Telegram API
   *
   * @return {Promise<void>}
   *
   * @private
   * @async
   */
  async _checkTelegramAPI() {
    const startTime = new Date();
    let botInfo;
    try {
      botInfo = await this._telegrafBot.telegram.getMe();
    } catch (e) {
      if (e.code === 'ENOTFOUND' || e.code === 'ECONNRESET') throw new Error('Telegram API error: Not available');
      if (e.code === 401) throw new Error('Telegram API error: Invalid token');
      throw new Error(`Telegram API error: ${e.message}`);
    }
    if (botInfo) {
      const endTime = new Date();
      this._logger.log(`Telegram API connection OK, ping: ${(endTime - startTime).toString()}ms`);
      if (process.env.BOT_NAME !== botInfo.username) throw new Error('Telegram API error: Wrong bot name configured');
      this._logger.log('Telegram API token check OK');
    }
  }

  /**
   * Check availability and consistence of database
   *
   * @return {Promise<void>}
   *
   * @private
   * @async
   */
  async _checkDb() {
    const startTime = new Date();
    let dbCheck;
    try {
      dbCheck = await this._dbMgr.checkStructure();
    } catch (e) {
      if (e.code === 'ECONNREFUSED') throw new Error('Database connection error: Database is not reachable');
      if (e.errno === 1044) throw new Error('Database connection error: DB user has no rights to the database');
      if (e.errno === 1049) throw new Error('Database consistency error: application database doesn\'t exist');
      throw new Error(`Database connection error: ${e.message}`);
    }
    const endTime = new Date();

    this._logger.log(`Database connection OK, ping: ${(endTime - startTime).toString()}ms`);

    if (dbCheck.chats && dbCheck.participants && dbCheck.hits) {
      this._logger.log('Database consistency OK');
    } else if (!dbCheck.chats && !dbCheck.participants && !dbCheck.hits) {
      this._logger.warn('Database might be not initialized (first start?)');
      this._logger.log('Database initialization started');

      try {
        await this._dbMgr.initStructure();
      } catch (e) {
        throw new Error(`Database initialization error: ${e.message}`);
      }

      this._logger.log('Database initialization completed');
    } else {
      this._logger.warn('Database consistency check failed: following tables were not found: ' +
        (!dbCheck.chats ? '"chats" ' : '') +
        (!dbCheck.participants ? '"participants" ' : '') +
        (!dbCheck.hits ? '"hits"' : '')
      );
      this._logger.log('Database restoration started');

      await this._dbMgr.restoreStructure({
        chats: dbCheck.chats,
        participants: dbCheck.participants,
        hits: dbCheck.hits
      });

      this._logger.log('Database restoration completed');
    }
  }

  /**
   * Configure custom middlewares for Telegraf bot
   *
   * @return {Promise<void>}
   *
   * @private
   * @async
   */
  async _configureMiddlewares() {
    this._telegrafBot.use(async (ctx, next) => {
      if (ctx.message.chat.type !== 'supergroup') return next();
      const msgTypeFiltered = ctx.message.entities?.filter(ent => ent.type === 'bot_command' && ent.offset === 0) || [];
      if (ctx.updateType === 'message' && msgTypeFiltered.length > 0) {
        const text = ctx.update.message.text.toLowerCase();

        if (text.startsWith('/') && /[A-Za-z]/.test(text.substr(1,1))) {
          const extractedCommand = text.split(' ')[0];
          const regex = new RegExp('^\\/([^\\s@]+)@' + process.env.BOT_NAME.toLowerCase());
          const match = extractedCommand.match(regex);
          const checkedCommand = (match && match[1]) ? match[1] : extractedCommand.substr(1);

          if (Object.values(BotCommands.literals).indexOf(checkedCommand) > -1) {
            this._logger.log(`Chat (TgID: ${ctx.message.chat.id}): +${checkedCommand}`);
            await next();
            this._logger.log(`Chat (TgID: ${ctx.message.chat.id}): -${checkedCommand}`);
          } else {
            this._logger.warn(`Chat (TgID: ${ctx.message.chat.id}): unknown command "${checkedCommand}" ignored`)
          }
        }
      }
    });
  }

  /**
   * Register shutdown event handlers
   *
   * @private
   */
  _registerShutdownEventHandlers() {
    process.on('SIGTERM', this._shutdownEventHandler.cleanup);
    process.on('SIGINT', this._shutdownEventHandler.cleanup);
  }
}

module.exports = BotManager;