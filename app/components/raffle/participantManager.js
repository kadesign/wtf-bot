const MicroLog = require('@kadesign/microlog').default;
const { escapeUnderscore } = require('@kadesign/toolbox-js');

const Message = require('../../models/Message');
const ErrorMessage = require('../../models/ErrorMessage');

/**
 * Participant manager.
 * Handles actions with participants
 *
 * @class
 *
 * @category Components
 * @subcategory Raffle
 */
class ParticipantManager {
  /**
   * Create ParticipantManager instance
   *
   * @param {DbManager}               dbMgr        Reference to application's {@link DbManager} instance
   * @param {module:MessageTemplates} msgTemplates Reference to application's {@link module:MessageTemplates MessageTemplates} module
   */
  constructor(dbMgr, msgTemplates) {
    /**
     * Logger instance
     *
     * @type {MicroLog}
     *
     * @private
     */
    this._logger = new MicroLog(this);

    /**
     * Reference to application's {@link DbManager} instance
     *
     * @type {DbManager}
     *
     * @private
     */
    this._dbMgr = dbMgr;

    /**
     * Reference to application's {@link module:MessageTemplates MessageTemplates} module
     *
     * @type {module:MessageTemplates}
     *
     * @private
     */
    this._msgTemplates = msgTemplates;
  }

  /**
   * Handle user's attempt to enter the raffle
   *
   * @param {number} chatTgId Chat's Telegram ID
   * @param {User}   tgUser   User instance (Telegraf.js library)
   *
   * @return {Promise<Message>} Message with the result of the attempt entering the raffle
   *
   * @public
   * @async
   */
  async enter(chatTgId, tgUser) {
    this._logger.log(`Chat (TgID: ${chatTgId}): user (TgID: ${tgUser.id}) tries to enter raffle`);

    if (!tgUser.username) {
      this._logger.warn(`Chat (TgID: ${chatTgId}): user (TgID: ${tgUser.id}) has no username, action declined`);

      return new Message(this._msgTemplates.errors.emptyUsername, null, true);
    }

    try {
      this._logger.log(`Chat (TgID: ${chatTgId}): trying to find chat in database`);

      let chat = await this._dbMgr.getChatInfo({ tgId: chatTgId });

      if (!chat) {
        chat = await this._dbMgr.createChat(chatTgId);

        this._logger.log(`Chat (TgID: ${chatTgId}): not found, new entry has been created with ID = ${chat.id}`);
      } else {
        this._logger.log(`Chat (TgID: ${chatTgId}): chat ID = ${chat.id}`);
      }

      this._logger.log(`Chat (TgID: ${chatTgId} / ID: ${chat.id}): trying to find participant in database`);
      let participant = await this._dbMgr.getChatParticipantByTgId(chat.id, tgUser.id);

      if (!participant) {
        participant = await this._dbMgr.createParticipant(tgUser.id, tgUser.username, chat.id);

        this._logger.log(`Chat (TgID: ${chatTgId} / ID: ${chat.id}): participant not found, new entry has been created with ID = ${participant.id}`);

        const count = await this._dbMgr.getChatParticipantCount(chat.id, true);

        return new Message(
          this._msgTemplates.commands.enter.ok,
          [escapeUnderscore(participant.username), count],
          true
        );
      } else {
        this._logger.log(`Chat (TgID: ${chatTgId} / ID: ${chat.id}): participant ID = ${participant.id}`);

        if (participant.username !== tgUser.username) {
          this._logger.log(`Chat (TgID: ${chatTgId} / ID: ${chat.id}): user "${participant.username}" (ID: ${participant.id}) has changed their username to "${tgUser.username}"`);
          await this._dbMgr.updateParticipantUsername(participant.id, tgUser.username);

          participant.username = tgUser.username;
        }

        if (participant.isActive) {
          this._logger.log(`Chat (TgID: ${chatTgId} / ID: ${chat.id}): user "${participant.username}" (ID: ${participant.id}) is already active`);

          return new Message(this._msgTemplates.commands.enter.alreadyPlaying, [escapeUnderscore(participant.username)], true);
        } else {
          this._logger.log(`Chat (TgID: ${chatTgId} / ID: ${chat.id}): user "${participant.username}" (ID: ${participant.id}) is inactive, activating`);
          await this._dbMgr.activateParticipant(participant.id);

          this._logger.log(`Chat (TgID: ${chatTgId} / ID: ${chat.id}): user "${participant.username}" (ID: ${participant.id}) is back in raffle`);

          const count = await this._dbMgr.getChatParticipantCount(chat.id, true);

          return new Message(
            this._msgTemplates.commands.enter.welcomeBack,
            [escapeUnderscore(participant.username), count],
            true
          );
        }
      }
    } catch (e) {
      this._logger.error(e);

      return new ErrorMessage();
    }
  }

  /**
   * Handle user's attempt to leave the raffle
   *
   * @param {number} chatTgId Chat's Telegram ID
   * @param {User}   tgUser   User instance (Telegraf.js library)
   *
   * @return {Promise<?Message>} Result of the attempt leaving the raffle. Null if chat is not present in the raffle
   *
   * @public
   * @async
   */
  async leave(chatTgId, tgUser) {
    this._logger.log(`Chat (TgID: ${chatTgId}): user (TgID: ${tgUser.id}) tries to leave raffle`);

    if (!tgUser.username) {
      this._logger.warn(`Chat (TgID: ${chatTgId}): user (TgID: ${tgUser.id}) has no username, action declined`);

      return new Message(this._msgTemplates.errors.emptyUsername, null, true);
    }

    this._logger.log(`Chat (TgID: ${chatTgId}): trying to find chat in database...`);

    try {
      let chat = await this._dbMgr.getChatInfo({ tgId: chatTgId });

      if (!chat) {
        this._logger.log(`Chat (TgID: ${chatTgId}): nothing to do here`);

        return null;
      } else {
        this._logger.log(`Chat (TgID: ${chatTgId}): chat ID = ${chat.id}`);
        this._logger.log(`Chat (TgID: ${chatTgId} / ID: ${chat.id}): trying to find participant in database`);

        let participant = await this._dbMgr.getChatParticipantByTgId(chat.id, tgUser.id);

        if (!participant || (participant && !participant.isActive)) {
          this._logger.log(`Chat (TgID: ${chatTgId} / ID: ${chat.id}): user "${tgUser.username}" (TgID: ${tgUser.id}) is not active participant${!participant ? ' and is not in database' : ''}`);

          return new Message(
            this._msgTemplates.commands.leave.notInGame,
            [escapeUnderscore(tgUser.username)],
            true
          );
        } else {
          await this._dbMgr.deactivateParticipant(participant.id);
          this._logger.log(`Chat (TgID: ${chatTgId} / ID: ${chat.id}): user "${participant.username}" (TgID: ${participant.id}) has left raffle`);

          return new Message(
            this._msgTemplates.commands.leave.ok,
            [escapeUnderscore(participant.username)],
            true
          );
        }
      }
    } catch (e) {
      this._logger.error(e);

      return new ErrorMessage();
    }
  }

  /**
   * Check if user which entered the chat has already took part in the raffle in the past
   *
   * @param {number} chatTgId Chat's Telegram ID
   * @param {User}   tgUser   User instance (Telegraf.js library)
   *
   * @return {Promise<?Message>} Welcome message. Null if the user didn't take part in the raffle yet
   *
   * @public
   * @async
   */
  async checkIfReturned(chatTgId, tgUser) {
    if (!tgUser.username) {
      this._logger.warn(`Chat (TgID: ${chatTgId}): user (TgID: ${tgUser.id}) has no username, checking previous actions declined`);

      return null;
    }

    try {
      this._logger.log(`Chat (TgID: ${chatTgId}): someone has entered chat, getting chat data`);

      let chat = await this._dbMgr.getChatInfo({ tgId: chatTgId });

      if (chat) {
        this._logger.log(`Chat (TgID: ${chatTgId}): ID = ${chat.id}`);
        this._logger.log(`Chat (TgID: ${chatTgId} / ID: ${chat.id}): user (TgID: ${tgUser.id}) has entered chat, checking their activity in the past`);

        let participant = await this._dbMgr.getChatParticipantByTgId(chat.id, tgUser.id);

        if (participant) {
          this._logger.log(`Chat (TgID: ${chatTgId} / ID: ${chat.id}): user "${participant.username}" (TgID: ${tgUser.id}, ID: ${participant.id}) took part in raffle`);

          if (participant.username !== tgUser.username) {
            this._logger.log(`Chat (TgID: ${chatTgId} / ID: ${chat.id}): user "${participant.username}" (ID: ${participant.id}) has changed their username to "${tgUser.username}"`);
            await this._dbMgr.updateParticipantUsername(participant.id, tgUser.username);

            participant.username = tgUser.username;
          }

          return new Message(
            this._msgTemplates.events.participantIsBack,
            [escapeUnderscore(participant.username)],
            true
          );
        } else {
          this._logger.log(`Chat (TgID: ${chatTgId} / ID: ${chat.id}): user "${tgUser.username}" (TgID: ${tgUser.id}) didn't take part in raffle`);
        }
      } else {
        this._logger.log(`Chat (TgID: ${chatTgId}): nothing to do here`);
      }
    } catch (e) {
      this._logger.error(e);

      return null;
    }
  }
}

module.exports = ParticipantManager;