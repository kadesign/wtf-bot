const MicroLog = require('@kadesign/microlog').default;
const { DateTime } = require('luxon');
const { escapeUnderscore, getRandomNumberBetween } = require('@kadesign/toolbox-js');

const Message = require('../../models/Message');
const MessageSequence = require('../../models/MessageSequence');
const ErrorMessage = require('../../models/ErrorMessage');
const RaffleTopParticipant = require('../../models/RaffleTopParticipant');

/**
 * Result of the raffle
 *
 * @typedef {object} GOTYRaffleRunner#RaffleResult
 *
 * @property {number} year         Year of raffle
 * @property {string} newWinner    Username of the new winner
 * @property {string} oldWinner    Username of the previous winner
 * @property {number} winnerPoints Points the user has won
 * @property {number} overtaken    Relation of the new winner to the previous winner (0 - win not overtaken, 1 - win overtaken, 2 - same winner)
 */

/**
 * "Gay of the Year" raffle runner
 *
 * @class
 *
 * @category Components
 * @subcategory Raffle
 */
class GOTYRaffleRunner {
  /**
   * Create GOTY raffle runner instance
   *
   * @param {DbManager}               dbMgr        Reference to application's {@link DbManager} instance
   * @param {module:MessageTemplates} msgTemplates Reference to application's {@link module:MessageTemplates MessageTemplates} module
   * @param {RaffleRunner}            raffleRunner Reference to application's {@link RaffleRunner} instance
   */
  constructor(dbMgr, msgTemplates, raffleRunner) {
    /**
     * Logger instance
     *
     * @type {MicroLog}
     *
     * @private
     */
    this._logger = new MicroLog(this);

    /**
     * Reference to application's {@link DbManager} instance
     *
     * @type {DbManager}
     *
     * @private
     */
    this._dbMgr = dbMgr;

    /**
     * Reference to application's {@link module:MessageTemplates MessageTemplates} module
     *
     * @type {module:MessageTemplates}
     *
     * @private
     */
    this._msgTemplates = msgTemplates;

    /**
     * Reference to application's {@link RaffleRunner} instance
     *
     * @type {RaffleRunner}
     *
     * @private
     */
    this._raffleRunner = raffleRunner;

    /**
     * Timezone for raffle
     *
     * @type {string}
     *
     * @private
     */
    this._timezone = raffleRunner.timezone;
  }

  /**
   * Run GOTY raffle
   *
   * @param {number} chatTgId   Chat's Telegram ID
   * @param {string} tgUsername Telegram username of participant which has started the raffle
   *
   * @return {Promise<Message|MessageSequence|ErrorMessage>} Error message or resulting message sequence
   *
   * @public
   * @async
   */
  async roll(chatTgId, tgUsername) {
    const chat = await this._dbMgr.getChatInfo({ tgId: chatTgId });

    if (!chat) {
      this._logger.warn(`Chat (TgID: ${chatTgId}): GOTY raffle can't be drawn: chat doesn't exist in database`);

      return new Message(this._msgTemplates.errors.notEnoughParticipants, null, true);
    }

    const now = DateTime.now().setZone(this._timezone);

    let earliestStartDate = await this._dbMgr.getChatEarliestGOTYRaffleDate(chat.id);
    earliestStartDate = DateTime.fromJSDate(earliestStartDate, { zone: this._timezone }).setLocale('ru').set({ year: now.year });
    this._logger.log(`Chat (TgID: ${chatTgId}): earliest GOTY raffle date is ${earliestStartDate.toFormat('yyyy-MM-dd')}`);

    if (now < earliestStartDate) {
      const formattedDate = earliestStartDate.toLocaleString({
        day: 'numeric',
        month: 'long'
      });
      return new Message(this._msgTemplates.commands.chooseWinner.tooEarly, [formattedDate],true);
    } else {
      if (!tgUsername) {
        this._logger.warn(`Chat (TgID: ${chatTgId}): user triggered draw has no username, action declined`);

        return new Message(this._msgTemplates.errors.emptyUsername, null, true);
      }

      try {
        const chat = await this._dbMgr.getChatInfo({ tgId: chatTgId });

        if (!chat) {
          this._logger.warn(`Chat (TgID: ${chatTgId}): can't get GOTY raffle winner: chat doesn't exist in database`);
          return new Message(this._msgTemplates.errors.notEnoughParticipants, null, true);
        }

        const thisYear = now.year;
        if (chat.lastGotyRaffleYear === now.year) {
          const user = await this._dbMgr.getParticipantById(chat.lastGotyRaffleWinnerId);

          if (user.username) {
            this._logger.log(`Chat (TgID: ${chat.tgId} / ID: ${chat.id}): winner was already chosen in this year - "${user.username}" (ID: ${chat.lastGotyRaffleWinnerId})`);

            return new Message(this._msgTemplates.commands.chooseWinner.chosen, [escapeUnderscore(user.username)], true);
          }
        }

        const currentTop = await this._dbMgr.getChatTop(chat.id, now.year);
        const currentTopUser = currentTop[0];

        const drawResult = await this._raffleRunner.determineWinner(chat.id, chat.tgId);
        if (drawResult instanceof Message) return drawResult;

        let winnerUser = currentTop.filter(fUser => fUser.id === drawResult.id)[0];
        const winnerExists = !!winnerUser;

        // winner didn't get points during the year
        if (!winnerExists) {
          winnerUser = new RaffleTopParticipant(drawResult.id, drawResult.username, 0);
        }

        const diff = await this._dbMgr.getChatCountDifference(chat.id, now.year);
        let rollPoints = getRandomNumberBetween(1, diff);
        let newWinnerScore = winnerUser.total + rollPoints;

        if (currentTopUser.id !== winnerUser.id && currentTopUser.total === newWinnerScore) {
          rollPoints++;
          newWinnerScore++;
        }

        /*
          0 - not overtaken
          1 - overtaken
          2 - same winner
        */
        let overtaken = 0;
        if (winnerUser.id !== currentTopUser.id && currentTopUser.total < newWinnerScore) {
          overtaken = 1;
        } else if (winnerUser.id === currentTopUser.id) {
          overtaken = 2;
        }

        const finalWinnerId = overtaken === 0 ? currentTopUser.id : drawResult.id;
        await this._dbMgr.updateChatRaffleData(now, finalWinnerId, chat.id);
        await this._dbMgr.updateChatGOTYRaffleData(now, finalWinnerId, chat.id);
        if (!winnerExists) {
          await this._dbMgr.createEmptyParticipantHitCounter(drawResult.id, chat.id, now.year);
        }
        await this._dbMgr.increaseParticipantHitCounter(drawResult.id, now.year, rollPoints);

        if (chat.lastRaffleWinnerId === drawResult.id) {
          drawResult.currentCombo++;
        } else {
          if (chat.lastRaffleWinnerId) {
            await this._dbMgr.updateParticipantCurrentCombo(chat.lastRaffleWinnerId, 0);
          }
          drawResult.currentCombo = 1;
        }

        if (drawResult.currentCombo > drawResult.maxCombo) {
          drawResult.maxCombo = drawResult.currentCombo;
        }

        await this._dbMgr.updateParticipantAllCombos(drawResult.id, drawResult.currentCombo, drawResult.maxCombo);

        return this._getMsgSequence({
          year: thisYear,
          newWinner: winnerUser.username,
          oldWinner: currentTopUser.username,
          winnerPoints: rollPoints,
          overtaken: overtaken
        });
      } catch (e) {
        this._logger.error(e);
        return new ErrorMessage();
      }
    }
  }

  /**
   * Construct the final message
   *
   * @param {GOTYRaffleRunner#RaffleResult} result Raffle result
   *
   * @return {Message} Final {@link Message Message}
   *
   * @private
   */
  _constructFinalMessage(result) {
    if (result.overtaken === 0) {
      return new Message(
        this._msgTemplates.commands.chooseWinner.determine.wonButNotFirst,
        [
          escapeUnderscore(result.newWinner),
          result.year.toString(),
          escapeUnderscore(result.oldWinner)
        ],
        true,
        0
      );
    } else if (result.overtaken === 1) {
      return new Message(
        this._msgTemplates.commands.chooseWinner.determine.epicWin,
        [escapeUnderscore(result.newWinner), result.year.toString()],
        true,
        0
      );
    } else if (result.overtaken === 2) {
      return new Message(
        this._msgTemplates.commands.chooseWinner.determine.wonButWasFirst,
        [escapeUnderscore(result.newWinner), result.year.toString()],
        true,
        0
      );
    } else {
      const err = new Error('Value of parameter "overtaken" is unknown');
      this._logger.error(err);
      return new ErrorMessage(err.message);
    }
  }

  /**
   * Get message sequence to be sent to the chat
   *
   * @param {GOTYRaffleRunner#RaffleResult} result Raffle result
   *
   * @return {MessageSequence} {@link MessageSequence} with all messages
   *
   * @private
   */
  _getMsgSequence(result) {
    return new MessageSequence([
      new Message(
        this._msgTemplates.commands.chooseWinner.determine.suspense[0],
        null,
        true,
        1000,
        true
      ),
      new Message(
        this._msgTemplates.commands.chooseWinner.determine.suspense[1],
        null,
        false,
        2000,
        true
      ),
      new Message(
        this._msgTemplates.commands.chooseWinner.determine.suspense[2],
        null,
        false,
        3000,
        true
      ),
      new Message(
        this._msgTemplates.commands.chooseWinner.determine.suspense[3],
        null,
        false,
        3000,
        true
      ),
      new Message(
        this._msgTemplates.commands.chooseWinner.determine.suspense[4],
        [escapeUnderscore(result.newWinner)],
        true,
        2000,
        true
      ),
      new Message(
        this._msgTemplates.commands.chooseWinner.determine.suspense[5],
        [result.newWinner],
        false,
        2000,
        true
      ),
      new Message(
        this._msgTemplates.commands.chooseWinner.determine.suspense[6],
        [result.winnerPoints.toString()],
        true,
        3000,
        true
      ),
      this._constructFinalMessage(result)
    ]);
  }
}

module.exports = GOTYRaffleRunner;