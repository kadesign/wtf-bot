const MicroLog = require('@kadesign/microlog').default;
const { DateTime } = require('luxon');
const { getRandomIndex, getRandomNumberBetween, escapeUnderscore, shuffleArray, isObjectEmpty } = require('@kadesign/toolbox-js');

const Message = require('../../models/Message');
const MessageSequence = require('../../models/MessageSequence');
const ErrorMessage = require('../../models/ErrorMessage');

/**
 * Regular raffle runner
 *
 * @class
 *
 * @category Components
 * @subcategory Raffle
 */
class RaffleRunner {
  /**
   * Create a raffle runner instance
   *
   * @param {Telegraf}                telegrafBot  Reference to bot instance (Telegraf.js library)
   * @param {DbManager}               dbMgr        Reference to application's {@link DbManager} instance
   * @param {module:MessageTemplates} msgTemplates Reference to application's {@link module:MessageTemplates MessageTemplates} module
   */
  constructor(telegrafBot, dbMgr, msgTemplates) {
    /**
     * Logger instance
     *
     * @type {MicroLog}
     *
     * @private
     */
    this._logger = new MicroLog(this);

    /**
     * Reference to bot instance (Telegraf.js library)
     *
     * @type {Telegraf}
     *
     * @private
     */
    this._telegrafBot = telegrafBot;

    /**
     * Reference to application's {@link DbManager} instance
     *
     * @type {DbManager}
     *
     * @private
     */
    this._dbMgr = dbMgr;

    /**
     * Reference to application's {@link module:MessageTemplates MessageTemplates} module
     *
     * @type {module:MessageTemplates}
     *
     * @private
     */
    this._msgTemplates = msgTemplates;

    /**
     * Timezone for raffle
     *
     * @type {string}
     *
     * @public
     */
    this.timezone = process.env.WTF_RAFFLE_TZ || 'UTC';
    this._logger.log(`Timezone is ${this.timezone}`);
  }

  /**
   * Determine the winner
   *
   * @param {number} chatId   Chat ID
   * @param {number} chatTgId Chat's Telegram ID
   *
   * @return {Promise<Participant|Message>} {@link Participant} which is selected as winner or {@link Message} with error details
   *
   * @throws Database errors
   *
   * @public
   * @async
   */
  async determineWinner (chatId, chatTgId) {
    let participants = await this._dbMgr.getChatParticipantList(chatId, true);

    this._logger.log(`Chat (TgID: ${chatTgId} / ID: ${chatId}): ${participants.length} active participant(s) found`);

    if (participants.length < 2) {
      this._logger.warn(`Chat (TgID: ${chatTgId} / ID: ${chatId}): raffle can't be drawn: not enough participants`);

      return new Message(this._msgTemplates.errors.notEnoughParticipants, null, true);
    }
    shuffleArray(participants);

    this._logger.log(`Chat (TgID: ${chatTgId} / ID: ${chatId}): randomly choosing a winner`);
    const winner = participants[getRandomIndex(participants)];
    const ignoredUsers = this._getIgnoredUsers();

    if (!isObjectEmpty(ignoredUsers) && ignoredUsers.hasOwnProperty(chatTgId) && ignoredUsers[chatTgId].indexOf(winner.tgId) > -1) {
      this._logger.log(`Chat (TgID: ${chatTgId} / ID: ${chatId}): user "${winner.username}" (ID: ${winner.id}) is configured to be ignored in this chat, re-rolling`);

      return await this.determineWinner(chatId, chatTgId);
    }
    this._logger.log(`Chat (TgID: ${chatTgId} / ID: ${chatId}): winner is "${winner.username}" (ID: ${winner.id})`);

    return winner;
  }

  /**
   * Check winner username and presence in chat.
   *
   * @param {Participant} userToCheck
   * @param {number}      chatTgId
   * @param {number}      chatId
   *
   * @return {Promise<boolean>} True if user is present in chat
   *
   * @throws Telegram API returns error with code != 400
   *
   * @private
   */
  async _checkWinner(userToCheck, chatTgId, chatId) {
    /*
     * UNUSED:
     * Telegram Bot API behaves unexpectedly since March 2020.
     * Method getChatMember returns error 400 even if user is in chat so we can't rely on this when checking presence in chat.
     */
    let userInfo;

    try {
      await this._telegrafBot.telegram.getChatMember(chatTgId, userToCheck.tgId);
    } catch (e) {
      if (e.code === 400) {
        this._logger.error(e);
        await this._dbMgr.deactivateParticipant(userToCheck.id);
        this._logger.warn(`Chat (TgID: ${chatTgId} / ID: ${chatId}): user "${userToCheck.username}" (ID: ${userToCheck.id}) left the chat, new winner should be chosen`);

        return false;
      } else {
        throw e;
      }
    }

    if (userInfo && userInfo.user.username !== userToCheck.username) {
      this._logger.log(`Chat (TgID: ${chatTgId} / ID: ${chatId}): user "${userToCheck.username}" (ID: ${userToCheck.id}) has changed their username to "${userInfo.user.username}", committing change to database`);
      await this._dbMgr.updateParticipantUsername(userToCheck.id, userInfo.user.username);

      userToCheck.username = userInfo.user.username;
    } else if (!userInfo) {
      this._logger.warn(`Chat (TgID: ${chatTgId} / ID: ${chatId}): skipping username check for user "${userToCheck.username}" (ID: ${userToCheck.id})`);
    }

    return true;
  }

  /**
   * Get list of ignored users
   *
   * @return {object} Object with keys as Telegram IDs of chats and values as arrays with Telegram IDs of the users
   *
   * @private
   * @ignore
   */
  _getIgnoredUsers() {
    const envVarRaw = process.env.WTF_SKIPUSERS ? process.env.WTF_SKIPUSERS.split(',') : [];
    const result = {};

    envVarRaw.forEach(el => {
      const userData = el.split(':');
      if (result[userData[0]]) {
        result[userData[0]].push(parseInt(userData[1]));
      } else {
        result[userData[0]] = [parseInt(userData[1])];
      }
    });

    return result;
  }

  /**
   * Run regular raffle
   *
   * @param chatTgId   Chat's Telegram ID
   * @param tgUsername Telegram username of participant which has started the raffle
   *
   * @return {Promise<Message|MessageSequence|ErrorMessage>} Error message or resulting message sequence
   *
   * @public
   * @async
   */
  async roll(chatTgId, tgUsername) {
    if (!tgUsername) {
      this._logger.warn(`Chat (TgID: ${chatTgId}): user triggered draw has no username, action declined`);

      return new Message(this._msgTemplates.errors.emptyUsername, null, true);
    }

    try {
      const chat = await this._dbMgr.getChatInfo({ tgId: chatTgId });

      if (!chat) {
        this._logger.warn(`Chat (TgID: ${chatTgId}): raffle can't be drawn: chat doesn't exist in database`);

        return new Message(this._msgTemplates.errors.notEnoughParticipants, null, true);
      }

      const now = DateTime.now().setZone(this.timezone);

      if (chat.lastGotyRaffleYear && chat.lastGotyRaffleYear === now.year) {
        const user = await this._dbMgr.getParticipantById(chat.lastGotyRaffleWinnerId);

        if (user.username) {
          this._logger.log(`Chat (TgID: ${chat.tgId} / ID: ${chat.id}): winner was already chosen in this year - "${user.username}" (ID: ${chat.lastGotyRaffleWinnerId})`);

          return new Message(
            this._msgTemplates.commands.chooseWinner.chosen,
            [escapeUnderscore(user.username)],
            true
          );
        }
      }

      if (chat.lastRaffleDate && now.hasSame(chat.lastRaffleDate, 'day')) {
        const user = await this._dbMgr.getParticipantById(chat.lastRaffleWinnerId);

        if (user.username) {
          this._logger.log(`Chat (TgID: ${chat.tgId} / ID: ${chat.id}): raffle was already drawn today, winner is "${user.username}" (ID: ${chat.lastRaffleWinnerId})`);
          return new Message(
            this._msgTemplates.commands.roll.exists,
            [escapeUnderscore(user.username)],
            true
          );
        }
      }

      const drawResult = await this.determineWinner(chat.id, chat.tgId);
      if (drawResult instanceof Message) return drawResult;

      let hits = await this._dbMgr.getParticipantHits(drawResult.id, now.year);
      if (!hits) {
        await this._dbMgr.createParticipantHitCounter(drawResult.id, chat.id, now.year);
      } else {
        await this._dbMgr.incrementParticipantHitCounter(drawResult.id, now.year);
      }
      await this._dbMgr.updateChatRaffleData(now, drawResult.id, chat.id);

      if (chat.lastRaffleWinnerId === drawResult.id) {
        drawResult.currentCombo++;
      } else {
        if (chat.lastRaffleWinnerId) {
          await this._dbMgr.updateParticipantCurrentCombo(chat.lastRaffleWinnerId, 0);
        }
        drawResult.currentCombo = 1;
      }

      if (drawResult.currentCombo > drawResult.maxCombo) {
        drawResult.maxCombo = drawResult.currentCombo;
      }

      await this._dbMgr.updateParticipantAllCombos(drawResult.id, drawResult.currentCombo, drawResult.maxCombo);

      return this._generateMsgSequence(drawResult);
    } catch (e) {
      this._logger.error(e);

      return new ErrorMessage();
    }
  }

  /**
   * Generate message sequence to be sent to the chat
   *
   * @param {Participant} winner {@link Participant} which won the raffle
   *
   * @return {MessageSequence} Resulting {@link MessageSequence}
   *
   * @private
   */
  _generateMsgSequence(winner) {
    const lastMsgTimeout = getRandomNumberBetween(2, 5) * 1000;
    const i = getRandomIndex(this._msgTemplates.commands.roll.determine);
    const sequence = [];

    this._msgTemplates.commands.roll.determine[i].forEach((message, index) => {
      if (index < this._msgTemplates.commands.roll.determine[i].length - 2) {
        sequence.push(new Message(message, null, true, 1000, true));
      } else if (index === this._msgTemplates.commands.roll.determine[i].length - 2) {
        sequence.push(new Message(message, null, true, lastMsgTimeout, true));
      } else {
        sequence.push(new Message(
          message,
          [escapeUnderscore(winner.username)],
          true,
          1000
        ));
      }
    });

    if (winner.currentCombo > 1) {
      sequence.push(new Message(
        this._msgTemplates.commands.roll.combo,
        [winner.currentCombo.toString()],
        true,
        1000
      ));
    }

    return new MessageSequence(sequence);
  }
}

module.exports = RaffleRunner;