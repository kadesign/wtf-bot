const util = require('util');
const MicroLog = require('@kadesign/microlog').default;
const { escapeUnderscore } = require('@kadesign/toolbox-js');

const Message = require('../../models/Message');
const ErrorMessage = require('../../models/ErrorMessage');
const {DateTime} = require('luxon');

/**
 * Stats manager.
 * Handles all stats requests
 *
 * @class
 *
 * @category Components
 * @subcategory Raffle
 */
class StatsManager {
  /**
   * Create a StatsManager instance
   *
   * @param {DbManager}               dbMgr        Reference to application's {@link DbManager} instance
   * @param {module:MessageTemplates} msgTemplates Reference to application's {@link module:MessageTemplates MessageTemplates} module
   * @param {RaffleRunner}            raffleRunner Reference to application's {@link RaffleRunner} instance
   */
  constructor(dbMgr, msgTemplates, raffleRunner) {
    /**
     * Logger instance
     *
     * @type {MicroLog}
     *
     * @private
     */
    this._logger = new MicroLog(this);

    /**
     * Reference to application's {@link DbManager} instance
     *
     * @type {DbManager}
     *
     * @private
     */
    this._dbMgr = dbMgr;

    /**
     * Reference to application's {@link module:MessageTemplates MessageTemplates} module
     *
     * @type {module:MessageTemplates}
     *
     * @private
     */
    this._msgTemplates = msgTemplates;

    /**
     * Timezone for stats
     *
     * @type {string}
     *
     * @private
     */
    this._timezone = raffleRunner.timezone;
  }

  /**
   * Get stats of chat for current year
   *
   * @param {number} chatTgId Chat's Telegram ID
   *
   * @return {Promise<Message|ErrorMessage>} Message with stats or error message
   *
   * @public
   * @async
   */
  async getCurrentYearStats(chatTgId) {
    this._logger.log(`Chat (TgID: ${chatTgId}): user is trying to get stats for current year`);

    try {
      this._logger.log(`Chat (TgID: ${chatTgId}): trying to find chat in database`);

      const chat = await this._dbMgr.getChatInfo({tgId: chatTgId});

      if (!chat) {
        this._logger.warn(`Chat (TgID: ${chatTgId}): not found in database`);

        return new Message(this._msgTemplates.commands.currentYearStats.empty, null, true);
      } else {
        this._logger.log(`Chat (TgID: ${chatTgId}): chat ID = ${chat.id}`);

        const currentYear = DateTime.now().setZone(this._timezone).year;
        this._logger.log(`Chat (TgID: ${chat.tgId} / ID: ${chat.id}): getting TOP for year ${currentYear}`);
        const top = await this._dbMgr.getChatTop(chat.id, currentYear);

        if (!top) {
          this._logger.warn(`Chat (TgID: ${chat.tgId} / ID: ${chat.id}): TOP for year ${currentYear} is empty`);

          return new Message(this._msgTemplates.commands.currentYearStats.empty, null, true);
        }

        this._logger.log(`Chat (TgID: ${chat.tgId} / ID: ${chat.id}): ${top.length} entries found`);
        this._logger.log(`Chat (TgID: ${chat.tgId} / ID: ${chat.id}): getting the winner of the year ${currentYear - 1}`);

        const lastGOTYWinner = await this._dbMgr.getChatGOTYWinner(chat.id, currentYear - 1);

        if (lastGOTYWinner) {
          this._logger.log(`Chat (TgID: ${chat.tgId} / ID: ${chat.id}): winner of the year ${currentYear - 1} is user (ID: ${lastGOTYWinner.id})`);
        } else {
          this._logger.log(`Chat (TgID: ${chat.tgId} / ID: ${chat.id}): no winner of the year ${currentYear - 1} found`);
        }

        let topHasCrown = false;
        let collectedTop = '';
        top.forEach((user, index) => {
          let crown = '';
          if (lastGOTYWinner) {
            if (user.id === lastGOTYWinner.id) crown = '🤴 ';
            topHasCrown = true;
          }
          collectedTop += '*' + (index + 1) + '.* ' + crown + escapeUnderscore(user.username) + ' — _' + user.total + 'pp_\n';
        });

        this._logger.log(`Chat (TgID: ${chat.tgId} / ID: ${chat.id}): getting count of active participants`);

        const count = await this._dbMgr.getChatParticipantCount(chat.id, true);

        this._logger.log(`Chat (TgID: ${chat.tgId} / ID: ${chat.id}): ${count} participants are active`);

        return new Message(
          this._msgTemplates.commands.currentYearStats.general,
          [
            currentYear,
            collectedTop,
            count,
            (topHasCrown ? this._msgTemplates.commands.currentYearStats.crown : '')
          ],
          true
        );
      }
    } catch (e) {
      this._logger.error(e);

      return new ErrorMessage();
    }
  }

  /**
   * Get overall stats of the chat
   *
   * @param {number} chatTgId Chat's Telegram ID
   *
   * @return {Promise<Message|ErrorMessage>} Message with stats or error message
   *
   * @public
   * @async
   */
  async getOverallStats(chatTgId) {
    this._logger.log(`Chat (TgID: ${chatTgId}): user is trying to get overall stats`);

    try {
      this._logger.log(`Chat (TgID: ${chatTgId}): trying to find chat in database`);
      const chat = await this._dbMgr.getChatInfo({ tgId: chatTgId });

      if (!chat) {
        this._logger.warn(`Chat (TgID: ${chatTgId}): not found in database`);

        return new Message(this._msgTemplates.commands.overallStats.empty, null, true);
      } else {
        this._logger.log(`Chat (TgID: ${chatTgId}): chat ID = ${chat.id}`);

        this._logger.log(`Chat (TgID: ${chat.tgId} / ID: ${chat.id}): getting overall TOP...`);
        const top = await this._dbMgr.getChatTop(chat.id);
        if (!top) {
          this._logger.warn(`Chat (TgID: ${chat.tgId} / ID: ${chat.id}): overall TOP is empty`);
          return new Message(this._msgTemplates.commands.overallStats.empty, null, true);
        }
        this._logger.log(`Chat (TgID: ${chat.tgId} / ID: ${chat.id}): ${top.length} entries found`);

        const currentYear = DateTime.now().setZone(this._timezone).year;

        this._logger.log(`Chat (TgID: ${chat.tgId} / ID: ${chat.id}): getting winner of the year ${currentYear - 1}`);
        const lastGOTYWinner = await this._dbMgr.getChatGOTYWinner(chat.id, currentYear - 1);
        if (lastGOTYWinner) {
          this._logger.log(`Chat (TgID: ${chat.tgId} / ID: ${chat.id}): winner of the year ${currentYear - 1} is user (ID: ${lastGOTYWinner.id})`);
        } else {
          this._logger.log(`Chat (TgID: ${chat.tgId} / ID: ${chat.id}): no winner of the year ${currentYear - 1} found`);
        }

        let topHasCrown = false;
        let collectedTop = '';
        top.forEach((user, index) => {
          let crown = '';
          if (lastGOTYWinner) {
            if (user.id === lastGOTYWinner.id) crown = '🤴 ';
            topHasCrown = true;
          }
          collectedTop += '*' + (index + 1) + '.* ' + crown + escapeUnderscore(user.username) + ' — _' + user.total + 'pp_\n';
        });

        return new Message(
          this._msgTemplates.commands.overallStats.general,
          [
            collectedTop,
            (topHasCrown ? this._msgTemplates.commands.overallStats.crown : '')
          ],
          true
        );
      }
    } catch (e) {
      this._logger.error(e);
      return new ErrorMessage();
    }
  }

  /**
   * Gets stats for particular participant
   *
   * @param {number} chatTgId Telegram ID of chat
   * @param {User}   tgUser   Instance of User which triggers getting his stats (Telegraf.js library)
   *
   * @return {Promise<Message|ErrorMessage>} Message with stats or error message
   *
   * @public
   * @async
   */
  async getParticipantStats(chatTgId, tgUser) {
    this._logger.log(`Chat (TgID: ${chatTgId}): user (TgID: ${tgUser.id}) is trying to get his stats`);

    if (!tgUser.username) {
      this._logger.warn("Chat (TgID: " + chatTgId + '): user (TgID: ' + tgUser.id + ') has no username, action declined');

      return new Message(this._msgTemplates.errors.emptyUsername, null, true);
    }

    try {
      this._logger.log(`Chat (TgID: ${chatTgId}): trying to find chat in database...`);

      const chat = await this._dbMgr.getChatInfo({ tgId: chatTgId });

      if (!chat) {
        this._logger.warn("Chat (TgID: " + chatTgId + '): not found in database');
        return new Message(this._msgTemplates.commands.userStats.notFound, [tgUser.username], true);
      } else {
        this._logger.log(`Chat (TgID: ${chatTgId}): chat ID = ${chat.id}`);

        const currentYear = DateTime.now().setZone(this._timezone).year;

        this._logger.log(`Chat (TgID: ${chat.tgId} / ID: ${chat.id}): getting stats for user (TgID: ${tgUser.id})`);
        const counters = await this._dbMgr.getParticipantStats(tgUser.id, chat.id, currentYear);
        if (!counters) {
          this._logger.warn(`Chat (TgID: ${chat.tgId} / ID: ${chat.id}): stats for user (TgID: ${tgUser.id}) are empty`);

          return new Message(this._msgTemplates.commands.userStats.empty, [escapeUnderscore(tgUser.username)], true);
        }

        this._logger.log(`Chat (TgID: ${chat.tgId} / ID: ${chat.id}): stats for user (TgID: ${tgUser.id}) found`);
        let currentComboMessage = counters.currentCombo > 1 ? util.format(this._msgTemplates.commands.userStats.combo, counters.currentCombo) : '';

        this._logger.log(`Chat (TgID: ${chat.tgId} / ID: ${chat.id}): getting ranks of user (TgID: ${tgUser.id})`);

        const ranks = await this._dbMgr.getParticipantRanks(chat.id, counters.id);

        let collectedRanks = '';
        if (ranks) {
          this._logger.log(`Chat (TgID: ${chat.tgId} / ID: ${chat.id}): ranks of user (TgID: ${tgUser.id}) found`);
          let winYears = ranks.filter(rank => rank.isWinner && rank.year !== currentYear).map(rank => rank.year).join(', ');
          if (winYears.length > 0) collectedRanks += util.format(this._msgTemplates.commands.userStats.ranks, winYears);
        } else {
          this._logger.log(`Chat (TgID: ${chat.tgId} / ID: ${chat.id}): ranks of user (TgID: ${tgUser.id}) not found`);
        }

        return new Message(
          this._msgTemplates.commands.userStats.text,
          [
            escapeUnderscore(counters.username),
            counters.currentYearHits,
            counters.totalHits,
            currentComboMessage,
            counters.maxCombo,
            collectedRanks,
            (!counters.isActive ? this._msgTemplates.commands.userStats.notActive : '')
          ],
          true
        );
      }
    } catch (e) {
      this._logger.error(e);

      return new ErrorMessage();
    }
  }

  /**
   * Gets maximal win streak rankings in the chat
   *
   * @param {number} chatTgId Telegram ID of chat
   *
   * @return {Promise<Message|ErrorMessage>} Message with stats or error message
   *
   * @public
   * @async
   */
  async getTopCombos(chatTgId) {
    this._logger.log(`Chat (TgID: ${chatTgId}): user from chat is trying to get max combo top`);

    try {
      this._logger.log(`Chat (TgID: ${chatTgId}): trying to find chat in database`);

      const chat = await this._dbMgr.getChatInfo({ tgId: chatTgId });
      if (!chat) {
        this._logger.warn(`Chat (TgID: ${chatTgId}): not found in database`);

        return new Message(this._msgTemplates.commands.topCombos.empty, null, true);
      } else {
        this._logger.log(`Chat (TgID: ${chatTgId}): chat ID = ${chat.id}`);

        this._logger.log(`Chat (TgID: ${chat.tgId} / ID: ${chat.id}): getting top combos...`);
        const top = await this._dbMgr.getChatTopCombos(chat.id);
        if (!top) {
          this._logger.warn(`Chat (TgID: ${chat.tgId} / ID: ${chat.id}): top combos is empty`);

          return new Message(this._msgTemplates.commands.topCombos.empty, null, true);
        }
        this._logger.log(`Chat (TgID: ${chat.tgId} / ID: ${chat.id}): ${top.length} entries found`);

        const currentYear = DateTime.now().setZone(this._timezone).year;

        this._logger.log(`Chat (TgID: ${chat.tgId} / ID: ${chat.id}): getting winner of the year ${currentYear - 1}`);
        const lastGOTYWinner = await this._dbMgr.getChatGOTYWinner(chat.id, currentYear - 1);
        if (lastGOTYWinner) {
          this._logger.log(`Chat (TgID: ${chat.tgId} / ID: ${chat.id}): winner of the year ${currentYear - 1} is user (ID: ${lastGOTYWinner.id})`);
        } else {
          this._logger.log(`Chat (TgID: ${chat.tgId} / ID: ${chat.id}): no winner of the year ${currentYear - 1} found`);
        }

        let topHasCrown = false;
        let collectedTop = '';
        top.forEach((user, index) => {
          let crown = '';
          if (lastGOTYWinner) {
            if (user.id === lastGOTYWinner.id) crown = '🤴 ';
            topHasCrown = true;
          }
          collectedTop += '*' + (index + 1) + '.* ' + crown + escapeUnderscore(user.username)
            + ' — _x' + user.total + '_\n';
        });

        return new Message(
          this._msgTemplates.commands.topCombos.general,
          [
            collectedTop,
            (topHasCrown ? this._msgTemplates.commands.topCombos.crown : '')
          ],
          true
        );
      }
    } catch (e) {
      this._logger.error(e);

      return new ErrorMessage();
    }
  }
}

module.exports = StatsManager;