const MicroLog = require('@kadesign/microlog').default;
const { DateTime } = require('luxon');

const Message = require('../../models/Message');
const ErrorMessage = require('../../models/ErrorMessage');

/**
 * Chat manager
 *
 * @class
 *
 * @category Components
 * @subcategory Raffle
 */
class ChatManager {
  /**
   * Create Chat manager instance
   *
   * @param {DbManager}               dbMgr        Reference to application's {@link DbManager} instance
   * @param {module:MessageTemplates} msgTemplates Reference to application's {@link module:MessageTemplates MessageTemplates} module
   * @param {RaffleRunner}            raffleRunner Reference to application's {@link RaffleRunner} instance
   */
  constructor(dbMgr, msgTemplates, raffleRunner) {
    /**
     * Logger instance
     *
     * @type {MicroLog}
     *
     * @private
     */
    this._logger = new MicroLog(this);

    /**
     * Reference to application's {@link DbManager} instance
     *
     * @type {DbManager}
     *
     * @private
     */
    this._dbMgr = dbMgr;

    /**
     * Reference to application's {@link module:MessageTemplates MessageTemplates} module
     *
     * @type {module:MessageTemplates}
     *
     * @private
     */
    this._msgTemplates = msgTemplates;

    /**
     * Timezone used for raffle
     *
     * @type {string}
     *
     * @private
     */
    this._timezone = raffleRunner.timezone;
  }

  /**
   * Handle user's attempt to change earliest GOTY raffle start date
   *
   * @param {number} chatTgId Chat's Telegram ID
   * @param {User}   tgUser   User instance (Telegraf.js library)
   * @param {string} text     Full message text
   *
   * @return {Promise<Message|ErrorMessage>} Error message or resulting message
   * @public
   * @async
   */
  async setGOTYDate(chatTgId, tgUser, text) {
    this._logger.log(`Chat (TgID: ${chatTgId}): user (TgID: ${tgUser.id}) is trying to see/change earliest GOTY raffle date`);
    if (!tgUser.username) {
      this._logger.warn(`Chat (TgID: ${chatTgId}): user (TgID: ${tgUser.id}) has no username, action declined`);

      return new Message(this._msgTemplates.errors.emptyUsername, null, true);
    }

    try {
      const chat = await this._dbMgr.getChatInfo({ tgId: chatTgId });

      if (!chat) {
        this._logger.warn("Chat (TgID: " + chatTgId + '): not found in database');
        return new Message(this._msgTemplates.commands.setGotyDate.chatNotFound, [tgUser.username], true);
      } else {
        let currentGOTYRaffleDate = await this._dbMgr.getChatEarliestGOTYRaffleDate(chat.id);
        currentGOTYRaffleDate = DateTime.fromJSDate(currentGOTYRaffleDate, {zone: this._timezone}).setLocale('ru');
        const newDateString = text.split(' ')[1];

        if (newDateString) {
          if (newDateString.match(/\d{1,2}\.\d{1,2}/)) {
            const newDateParts = newDateString.split('.').map(num => parseInt(num, 10));
            const newDate = DateTime.fromObject({ day: newDateParts[0], month: newDateParts[1] }, { zone: this._timezone, locale: 'ru' });
            if (newDate.isValid) {
              if (newDate.hasSame(currentGOTYRaffleDate, 'day')) {
                this._logger.log(`Chat (TgID: ${chat.tgId} / ID: ${chat.id}): date ${newDate.toFormat('dd.MM')} is already set`);

                return new Message(
                  this._msgTemplates.commands.setGotyDate.alreadySet,
                  [tgUser.username],
                  true
                );
              } else {
                this._logger.log(`Chat (TgID: ${chat.tgId} / ID: ${chat.id}): changing earliest GOTY raffle date to ${newDate.toFormat('dd.MM')}`);
                const formattedDate = newDate.toLocaleString({
                  day: 'numeric',
                  month: 'long'
                });

                await this._dbMgr.updateChatEarliestGOTYRaffleDate(chat.id, newDate);

                return new Message(
                  this._msgTemplates.commands.setGotyDate.success,
                  [formattedDate],
                  true
                );
              }
            } else {
              this._logger.log(`Chat (TgID: ${chat.tgId} / ID: ${chat.id}): date ${newDateString} is invalid`);
              return new Message(
                this._msgTemplates.commands.setGotyDate.invalidDate,
                [tgUser.username],
                true
              );
            }
          } else {
            this._logger.log(`Chat (TgID: ${chat.tgId} / ID: ${chat.id}): date ${newDateString} is invalid`);
            return new Message(
              this._msgTemplates.commands.setGotyDate.invalidDate,
              [tgUser.username],
              true
            );
          }
        } else {
          this._logger.log(`Chat (TgID: ${chat.tgId} / ID: ${chat.id}): current earliest GOTY raffle date is ${currentGOTYRaffleDate.toFormat('dd.MM')}`);
          const formattedDate = currentGOTYRaffleDate.toLocaleString({
            day: 'numeric',
            month: 'long'
          });

          return new Message(
            this._msgTemplates.commands.setGotyDate.ask,
            [formattedDate],
            true
          );
        }
      }
    } catch (e) {
      this._logger.error(e);
      return new ErrorMessage();
    }
  }
}

module.exports = ChatManager;