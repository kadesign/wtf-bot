/**
 * @module BotCommands
 * @desc Bot commands
 *
 * @category Components
 */
const commands = [
  {
    command: 'rules',
    description: 'Правила игры',
    internalName: 'rules'
  },
  {
    command: 'wannabegay',
    description: 'Вступить в игру',
    internalName: 'enter'
  },
  {
    command: 'pidorify',
    description: 'Раскрутить Пидор-рулетку',
    internalName: 'roll'
  },
  {
    command: 'gayoftheyear',
    description: 'Выбрать Пидора этого года',
    internalName: 'chooseWinner'
  },
  {
    command: 'finaldate',
    description: 'Посмотреть/поменять дату выбора Пидора года',
    internalName: 'setGotyDate'
  },
  {
    command: 'mystats',
    description: 'Твои достижения',
    internalName: 'userStats'
  },
  {
    command: 'top',
    description: 'Статистика за этот год',
    internalName: 'currentYearStats'
  },
  {
    command: 'allstars',
    description: 'Статистика за все время',
    internalName: 'overallStats'
  },
  {
    command: 'combos',
    description: 'Статистика по комбо за все время',
    internalName: 'topCombos'
  },
  {
    command: 'imstraight',
    description: 'Выйти из игры',
    internalName: 'leave'
  },
  {
    command: 'help',
    description: 'Описание всех команд',
    internalName: 'help'
  },
];

/**
 * Telegram-friendly list of commands
 *
 * @type {BotCommand[]}
 */
module.exports.toTelegramFormat = commands.map(cmd => ({
  command: cmd.command,
  description: cmd.description
}));

/**
 * Command literals.
 *
 * Keys are internal names, values are actual command literals.
 *
 * @type {{}}
 */
module.exports.literals = commands.reduce((result, cmd) => {
  result[cmd.internalName] = cmd.command;
  return result;
}, {});
