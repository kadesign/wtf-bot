const MicroLog = require('@kadesign/microlog').default;

const MessageTemplates = require('./messageTemplates');
const BotCommands = require('./botCommands');
const RaffleRunner = require('./raffle/raffleRunner');
const GOTYRaffleRunner = require('./raffle/gotyRaffleRunner');
const ParticipantManager = require('./raffle/participantManager');
const StatsManager = require('./raffle/statsManager');
const ChatManager = require('./raffle/chatManager');

const Message = require('../models/Message');
const MessageSequence = require('../models/MessageSequence');

let { sendMessage, sendMessageSequence } = require('../utils/telegramApiHelper');

/**
 * Command handler.
 * Executes necessary logic on registered commands
 *
 * @class
 *
 * @category Components
 */
class CommandsHandler {
  /**
   * Creates CommandHandler instance
   *
   * @param {Telegraf}  telegrafBot Reference to Telegraf bot instance (Telegraf.js library)
   * @param {DbManager} dbMgr       Reference to application's {@link DbManager} instance
   */
  constructor(telegrafBot, dbMgr) {
    /**
     * Logger instance
     *
     * @type {MicroLog}
     *
     * @private
     */
    this._logger = new MicroLog(this);

    /**
     * Reference to Telegraf bot instance (Telegraf.js library)
     *
     * @type {Telegraf}
     *
     * @private
     */
    this._telegrafBot = telegrafBot;

    /**
     * Reference to application's {@link DbManager} instance
     *
     * @type {DbManager}
     *
     * @private
     */
    this._dbMgr = dbMgr;

    /**
     * {@link RaffleRunner} instance
     *
     * @type {RaffleRunner}
     *
     * @private
     */
    this._raffleRunner = new RaffleRunner(this._telegrafBot, this._dbMgr, MessageTemplates);

    /**
     * {@link GOTYRaffleRunner} instance
     *
     * @type {GOTYRaffleRunner}
     *
     * @private
     */
    this._gotyRaffleRunner = new GOTYRaffleRunner(this._dbMgr, MessageTemplates, this._raffleRunner);

    /**
     * {@link ParticipantManager} instance
     *
     * @type {ParticipantManager}
     *
     * @public
     */
    this._participantManager = new ParticipantManager(this._dbMgr, MessageTemplates);

    /**
     * {@link StatsManager} instance
     *
     * @type {StatsManager}
     *
     * @private
     */
    this._statsManager = new StatsManager(this._dbMgr, MessageTemplates, this._raffleRunner);

    /**
     * {@link ChatManager} instance
     *
     * @type {ChatManager}
     *
     * @private
     */
    this._chatManager = new ChatManager(this._dbMgr, MessageTemplates, this._raffleRunner);
  }

  /**
   * Register handlers for all configured commands
   *
   * @public
   */
  register() {
    this._telegrafBot.command(BotCommands.literals.enter, async ctx => {
      const message = await this._participantManager.enter(ctx.message.chat.id, ctx.message.from);
      if (message) sendMessage(ctx, message, this._logger);
    });

    this._telegrafBot.command(BotCommands.literals.leave, async ctx => {
      const message = await this._participantManager.leave(ctx.message.chat.id, ctx.message.from);
      if (message) sendMessage(ctx, message, this._logger);
    });

    this._telegrafBot.command(BotCommands.literals.roll, async ctx => {
      const result = await this._raffleRunner.roll(ctx.message.chat.id, ctx.from.username);
      if (result instanceof Message) {
        sendMessage(ctx, result, this._logger);
      } else if (result instanceof MessageSequence) {
        sendMessageSequence(ctx, result, this._logger);
      }
    });

    this._telegrafBot.command(BotCommands.literals.chooseWinner, async ctx => {
      const result = await this._gotyRaffleRunner.roll(ctx.message.chat.id, ctx.from.username);
      if (result instanceof Message) {
        sendMessage(ctx, result, this._logger);
      } else if (result instanceof MessageSequence) {
        sendMessageSequence(ctx, result, this._logger);
      }
    });

    this._telegrafBot.command(BotCommands.literals.currentYearStats, async ctx => {
      const message = await this._statsManager.getCurrentYearStats(ctx.message.chat.id);
      sendMessage(ctx, message, this._logger);
    });

    this._telegrafBot.command(BotCommands.literals.overallStats, async ctx => {
      const message = await this._statsManager.getOverallStats(ctx.message.chat.id);
      sendMessage(ctx, message, this._logger);
    });

    this._telegrafBot.command(BotCommands.literals.userStats, async ctx => {
      const message = await this._statsManager.getParticipantStats(ctx.message.chat.id, ctx.message.from);
      sendMessage(ctx, message, this._logger);
    });

    this._telegrafBot.command(BotCommands.literals.topCombos, async ctx => {
      const message = await this._statsManager.getTopCombos(ctx.message.chat.id);
      sendMessage(ctx, message, this._logger);
    });

    this._telegrafBot.command(BotCommands.literals.setGotyDate, async ctx => {
      const message = await this._chatManager.setGOTYDate(ctx.message.chat.id, ctx.message.from, ctx.message.text);
      sendMessage(ctx, message, this._logger);
    });

    this._telegrafBot.command(BotCommands.literals.rules, ctx => {
      sendMessage(ctx, new Message(MessageTemplates.commands.rules, null, true), this._logger);
    });

    this._telegrafBot.command(BotCommands.literals.help, ctx => {
      sendMessage(ctx, new Message(MessageTemplates.commands.help, null, true), this._logger);
    });
  }

  async updateTelegramCommandList() {
    await this._telegrafBot.telegram.setMyCommands(BotCommands.toTelegramFormat);
  }
}

module.exports = CommandsHandler;