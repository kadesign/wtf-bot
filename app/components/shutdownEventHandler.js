const MicroLog = require('@kadesign/microlog').default;

/**
 * Shutdown event handler.
 * Contains cleanup methods to be executed on application's shutdown
 *
 * @class
 *
 * @category Components
 */
class ShutdownEventHandler {
  /**
   * Creates ShutdownEventHandler instance
   */
  constructor() {
    /**
     * Timeout before shutting down the application forcefully
     *
     * @type {number}
     *
     * @private
     * @const
     */
    this.FORCE_SHUTDOWN_TIMEOUT = 10000;
  }

  /**
   * Performs necessary cleanup
   *
   * @param {string} exitCode Application's exit code
   *
   * @return {Promise<void>}
   *
   * @public
   * @async
   */
  async cleanup(exitCode) {
    const logger = new MicroLog('ShutdownEventHandler');

    logger.log(`Kill signal ${exitCode} received, shutting down ${process.env.BOT_NAME}...`);
    setTimeout(() => {
      logger.warn('Can\'t clean up gracefully, forcefully shutting down the application');
      process.exit(1);
    }, this.FORCE_SHUTDOWN_TIMEOUT);

    // Some cleanup here if necessary

    logger.log(`${process.env.BOT_NAME} is closed.`);
    process.exit(0);
  }
}

module.exports = ShutdownEventHandler;