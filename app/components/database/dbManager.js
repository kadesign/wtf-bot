const MicroLog = require('@kadesign/microlog').default;
const { PrismaClient } = require('@prisma/client');
const { checkEnvVariables, orderBy } = require('@kadesign/toolbox-js');

const schema = require('./schema');

const Chat = require('../../models/Chat');
const Participant = require('../../models/Participant');
const ParticipantRank = require('../../models/ParticipantRank');
const RaffleTopParticipant = require('../../models/RaffleTopParticipant');

/**
 * Database state
 *
 * @typedef {object} DbManager#DbState
 *
 * @property {boolean} chats        True if table "chats" exists
 * @property {boolean} participants True if table "participants" exists
 * @property {boolean} hits         True if table "hits" exists
 */

/**
 * Database manager.
 * Contains methods for querying the database - reading and updating data.
 *
 * @class
 *
 * @category Components
 * @subcategory Database
 */
class DbManager {
  /**
   * Check presence of necessary configuration and create DbManager instance
   */
  constructor() {
    this.logger = new MicroLog(this);

    this._prisma = new PrismaClient({
      log: process.env.NODE_ENV === 'development' ? [ 'query' ] : null,
    });
  }

  /**
   * Check structure of database
   *
   * @return {Promise<DbManager#DbState>} State of database tables
   *
   * @public
   * @async
   */
  async checkStructure() {
    return {
      chats: await this._checkTableExist('chats'),
      participants: await this._checkTableExist('participants'),
      hits: await this._checkTableExist('hits')
    }
  }

  /**
   * Initialize structure of database
   *
   * @return {Promise<void>}
   *
   * @public
   * @async
   */
  async initStructure() {
    await this._createTable('chats');
    await this._createTable('participants');
    await this._createTable('hits');
  }

  /**
   * Restore corrupted structure of database
   *
   * @param {DbManager#DbState} dbState State of tables
   *
   * @return {Promise<void>}
   *
   * @public
   * @async
   */
  async restoreStructure(dbState) {
    if (!dbState.chats) {
      await this._createTable('chats');

      if (dbState.participants) {
        await this._prisma.participant.deleteMany();
      } else {
        await this._createTable('participants');
      }

      if (dbState.hits) {
        await this._prisma.hit.deleteMany();
      } else {
        await this._createTable('hits');
      }
    } else {
      if (dbState.participants) {
        if (dbState.hits) return;
        await this._prisma.participant.updateMany({
          data: {
            currentCombo: 0,
            maxCombo: 0
          }
        });
      } else {
        await this._createTable('participants');
      }

      if (dbState.hits) {
        if (dbState.participants) return;
        await this._prisma.hit.deleteMany();
      } else {
        await this._createTable('hits');
      }

      await this._prisma.chat.updateMany({
        data: {
          lastRaffleDate: null,
          lastRaffleWinnerId: null,
          lastGotyRaffleDate: null,
          lastGotyRaffleWinnerId: null
        }
      });
    }
  }

  /**
   * Get count of chats from the database
   *
   * @return {Promise<number>} Count of chats in the database
   *
   * @public
   * @async
   */
  async getChatsCount() {
    return await this._prisma.chat.count();
  }

  /**
   * Get chat information by its ID (internal or Telegram)
   *
   * @param {object} chatId        Chat ID. One of a kind should be provided
   * @param {number} [chatId.id]   Internal chat ID
   * @param {number} [chatId.tgId] Chat's Telegram ID
   *
   * @return {Promise<?Chat>} {@link Chat} instance with chat information
   *
   * @public
   * @async
   */
  async getChatInfo({id, tgId}) {
    let response;

    if (id && !tgId) {
      response = await this._prisma.chat.findUnique({
        where: { id }
      });
    } else {
      response = await this._prisma.chat.findUnique({
        where: { tgId }
      });
    }

    return response ? new Chat(
      response.id,
      response.tgId,
      response.lastRaffleWinnerId,
      response.lastRaffleDate,
      response.lastGotyRaffleWinnerId,
      response.lastGotyRaffleDate
    ) : null;
  }

  /**
   * Get top 10 participants from chat (overall or by year)
   *
   * @param {number} chatId Chat ID
   * @param {number} [year] Year for the request
   *
   * @return {Promise<?RaffleTopParticipant[]>} Array of {@link RaffleTopParticipant} instances for each participant
   *
   * @public
   * @async
   */
  async getChatTop(chatId, year) {
    let top = [];

    if (year) {
      const result = await this._prisma.hit.findMany({
        where: {
          year,
          participant: {
            is: {
              chatId,
              isActive: true
            }
          }
        },
        select: {
          count: true,
          participant: {
            select: {
              id: true,
              username: true
            }
          }
        },
        orderBy: [
          { count: 'desc' },
          {
            participant: { username: 'asc' }
          }
        ],
        take: 10
      });

      top = result.map(entry => ({
        userId: entry.participant.id,
        username: entry.participant.username,
        total: entry.count
      }));
    } else {
      const result = await this._prisma.hit.findMany({
        where: {
          participant: {
            is: { chatId }
          }
        },
        select: {
          count: true,
          participant: {
            select: {
              id: true,
              username: true
            }
          }
        }
      }).then(res => res.map(entry => ({
        userId: entry.participant.id,
        username: entry.participant.username,
        total: entry.count
      })));
      result.forEach(entry => {
        const existingEntry = top.filter(e => e.userId === entry.userId);
        if (existingEntry.length > 0) {
          existingEntry[0].total += entry.total;
        } else {
          top.push(entry);
        }
      });

      top.sort(orderBy(['total', 'username'], ['desc', 'asc']));
    }

    if (top.length > 0) {
      const topParticipants = [];
      top.forEach(entry => {
        topParticipants.push(new RaffleTopParticipant(entry.userId, entry.username, entry.total));
      });
      return topParticipants;
    } else {
      return null;
    }
  }

  /**
   * Get raffle leader and their total win count by year
   *
   * @param {number} chatId Chat ID
   * @param {number} year   Year for querying top participant
   *
   * @return {Promise<?RaffleTopParticipant>} {@link RaffleTopParticipant} instance for raffle leader
   *
   * @public
   * @async
   */
  async getChatGOTYWinner(chatId, year) {
    const leader = await this._prisma.hit.findFirst({
      select: {
        userId: true,
        count: true
      },
      where: { chatId, year },
      orderBy: { count: 'desc' }
    });

    return leader
      ? new RaffleTopParticipant(leader.userId, null, leader.count)
      : null;
  }

  /**
   * Get difference in total counts between raffle leader and outsider in particular year
   *
   * @param {number} chatId Chat ID
   * @param {number} year   Year for querying difference
   *
   * @return {Promise<?number>} Difference in total counts.
   *                            Returns null if there are no active participants in chat
   *
   * @public
   * @async
   */
  async getChatCountDifference(chatId, year) {
    const chatParticipantIds = await this._prisma.participant.findMany({
      select: { id: true },
      where: {
        isActive: true,
        chatId
      }
    }).then(result => result.map(entry => entry.id));
    if (chatParticipantIds.length === 0) return null;

    const diff = await this._prisma.hit.aggregate({
      _max: { count: true },
      _min: { count: true },
      where: {
        year,
        userId: { in: chatParticipantIds }
      }
    });

    if (diff._max.count === null) return 0;
    return diff._max.count - diff._min.count;
  }

  /**
   * Get list of participants in given chat
   *
   * @param {number}  chatId     Chat ID
   * @param {boolean} onlyActive If true return list of only active participants
   *
   * @return {Promise<Participant[]>} Array of {@link Participant} instances for each participant
   *
   * @public
   * @async
   */
  async getChatParticipantList(chatId, onlyActive = false) {
    const whereClause = onlyActive
      ? { chatId, isActive: true }
      : { chatId };

    return await this._prisma.participant.findMany({ where: whereClause })
      .then(results => results.map(participant => new Participant(
        participant.id,
        participant.tgId,
        participant.username,
        participant.chatId,
        participant.isActive,
        participant.currentCombo,
        participant.maxCombo
      )));
  }

  /**
   * Get count of participants in given chat
   *
   * @param {number}  chatId     Chat ID
   * @param {boolean} onlyActive If true return count of only active participants
   *
   * @return {Promise<number>} Count of participants
   *
   * @public
   * @async
   */
  async getChatParticipantCount(chatId, onlyActive) {
    const whereClause = onlyActive
      ? { chatId, isActive: true }
      : { chatId };

    return await this._prisma.participant.count({ where: whereClause });
  }

  /**
   * Get participant information by their ID
   *
   * @param {number} id Participant ID
   *
   * @return {Promise<?Participant>} {@link Participant} instance with participant information
   *
   * @public
   * @async
   */
  async getParticipantById(id) {
    return await this._prisma.participant.findUnique({
      where: { id }
    }).then(participant => participant
      ? new Participant(
          participant.id,
          participant.tgId,
          participant.username,
          participant.chatId,
          participant.isActive,
          participant.currentCombo,
          participant.maxCombo
        )
      : null);
  }

  /**
   * Get participant information by chat ID and their Telegram ID
   *
   * @param {number} chatId          Chat ID
   * @param {number} participantTgId Participant's Telegram ID
   *
   * @return {Promise<?Participant>} {@link Participant} instance with participant information
   *
   * @public
   * @async
   */
  async getChatParticipantByTgId(chatId, participantTgId) {
    return await this._prisma.participant.findFirst({
      where: {
        chatId,
        tgId: participantTgId
      }
    }).then(participant => participant
      ? new Participant(
        participant.id,
        participant.tgId,
        participant.username,
        participant.chatId,
        participant.isActive,
        participant.currentCombo,
        participant.maxCombo
      )
      : null);
  }

  /**
   * Get count of wins by participant in given year
   *
   * @param {number} userId Participant ID
   * @param {number} year   Year for querying count of participant wins
   *
   * @return {Promise<?number>} Count of wins by participant
   *
   * @public
   * @async
   */
  async getParticipantHits(userId, year) {
    return await this._prisma.hit.findFirst({
      select: { count: true },
      where: { userId, year }
    }).then(hit => hit ? hit.count : null);
  }

  /**
   * Get personal stats for participant in given year
   *
   * @param {number} tgId   Participant's Telegram ID
   * @param {number} chatId Chat ID
   * @param {number} year   Year for querying results
   *
   * @return {Promise<?Participant>} {@link Participant} instance with stats of participant
   *
   * @public
   * @async
   */
  async getParticipantStats(tgId, chatId, year) {
    const participant = await this._prisma.participant.findFirst({
      where: { tgId, chatId }
    });
    if (!participant) return null;

    const hits = await this._prisma.hit.findMany({
      where: {
        userId: participant.id,
        chatId
      }
    });
    if (hits.length === 0) return null;

    const totalHits = hits.reduce(((accumulator, currentHit) => accumulator + currentHit.count), 0);
    let currentYearHits = hits.filter(hit => hit.year === year);
    currentYearHits = currentYearHits.length > 0 ? currentYearHits[0].count : 0;

    return new Participant(
      participant.id,
      participant.tgId,
      participant.username,
      participant.chatId,
      participant.isActive,
      participant.currentCombo,
      participant.maxCombo,
      totalHits,
      currentYearHits
    );
  }

  /**
   * Get ranks for participant
   *
   * @param {number} userId Participant ID
   * @param {number} chatId Chat ID
   *
   * @return {Promise<?ParticipantRank[]>} Array of {@link ParticipantRank} instances for each rank
   *
   * @public
   * @async
   */
  async getParticipantRanks(userId, chatId) {
    const maxHits = await this._prisma.hit.groupBy({
      by: [ 'year', 'chatId' ],
      having: {
        chatId
      },
      _max: { count: true },
    });
    const participantCounters = await this._prisma.hit.findMany({
      select: {
        count: true,
        year: true
      },
      where: { userId }
    });
    if (participantCounters.length === 0) return null;

    return participantCounters.map(counter => {
      const yearMaxHits = maxHits.filter(hit => hit.year === counter.year)[0];
      return new ParticipantRank(userId, counter.year, counter.count === yearMaxHits._max.count);
    }).sort(orderBy( ['year'], ['asc'] ));
  }

  /**
   * Get maximal win streak rankings for chat
   *
   * @param {number} chatId Chat ID
   *
   * @return {Promise<?RaffleTopParticipant[]>} Array of {@link RaffleTopParticipant} instances for each participant
   *
   * @public
   * @async
   */
  async getChatTopCombos(chatId) {
    return await this._prisma.participant.findMany({
      select: {
        id: true,
        username: true,
        maxCombo: true,
      },
      where: {
        chatId,
        maxCombo: { gt: 0 }
      },
      orderBy: [
        { maxCombo: 'desc' },
        { username: 'asc' }
      ],
      take: 25
    }).then(results => results.length > 0
      ? results.map(ranking => new RaffleTopParticipant(
          ranking.id,
          ranking.username,
          ranking.maxCombo
        ))
      : null);
  }

  /**
   * Get earliest GOTY raffle date for chat
   *
   * @param {number} id Chat ID
   *
   * @return {Promise<?Date>} Earliest GOTY raffle date
   *
   * @public
   * @async
   */
  async getChatEarliestGOTYRaffleDate(id) {
    return await this._prisma.chat.findFirst({
      select: {
        earliestGotyRaffleDate: true
      },
      where: { id }
    }).then(result => result ? result.earliestGotyRaffleDate : null);
  }

  /**
   * Create chat in database
   *
   * @param {number} tgId Telegram ID of chat
   *
   * @return {Promise<Chat>} {@link Chat} instance for created chat
   *
   * @public
   * @async
   */
  async createChat(tgId) {
    return await this._prisma.chat.create({
      data: { tgId }
    }).then(result => new Chat(result.id, result.tgId));
  }

  /**
   * Create participant in database
   *
   * @param {number} tgId     Participant's Telegram ID
   * @param {string} username Participant's username
   * @param {number} chatId   Chat ID
   *
   * @return {Promise<Participant>} {@link Participant} instance of created participant
   *
   * @public
   * @async
   */
  async createParticipant(tgId, username, chatId) {
    return await this._prisma.participant.create({
      data: { tgId, username, chatId }
    }).then(result => new Participant(result.id, result.tgId, result.username, result.chatId, true));
  }

  /**
   * Update chat entry with last raffle information
   *
   * @param {DateTime} date     Date of raffle
   * @param {number}   winnerId Raffle winner ID
   * @param {number}   chatId   Chat ID
   *
   * @return {Promise<void>}
   *
   * @public
   * @async
   */
  async updateChatRaffleData(date, winnerId, chatId) {
    await this._prisma.chat.update({
      where: { id: chatId },
      data: {
        lastRaffleDate: date.toJSDate(),
        lastRaffleWinnerId: winnerId
      }
    });
  }

  /**
   * Update chat entry with last GOTY raffle information
   *
   * @param {DateTime} date     Date of GOTY raffle
   * @param {number}   winnerId Raffle winner ID
   * @param {number}   chatId   Chat ID
   *
   * @return {Promise<void>}
   *
   * @public
   * @async
   */
  async updateChatGOTYRaffleData(date, winnerId, chatId) {
    await this._prisma.chat.update({
      where: { id: chatId },
      data: {
        lastGotyRaffleDate: date.toJSDate(),
        lastGotyRaffleWinnerId: winnerId
      }
    });
  }

  /**
   * Update chat entry with earliest GOTY raffle date
   *
   * @param {number}   id   Chat ID
   * @param {DateTime} date Earliest date of GOTY raffle
   *
   * @return {Promise<void>}
   *
   * @public
   * @async
   */
  async updateChatEarliestGOTYRaffleDate(id, date) {
    await this._prisma.chat.update({
      where: { id },
      data: {
        earliestGotyRaffleDate: date.toJSDate()
      }
    });
  }

  /**
   * Deactivate a participant
   *
   * @param {number} id Participant ID
   *
   * @return {Promise<void>}
   *
   * @public
   * @async
   */
  async deactivateParticipant(id) {
    await this._prisma.participant.update({
      where: { id },
      data: { isActive: false }
    });
  }

  /**
   * Activate a participant
   *
   * @param {number} id Participant ID
   *
   * @return {Promise<void>}
   *
   * @public
   * @async
   */
  async activateParticipant(id) {
    await this._prisma.participant.update({
      where: { id },
      data: { isActive: true }
    });
  }

  /**
   * Update username of a participant
   *
   * @param {number} id       Participant ID
   * @param {string} username New username
   *
   * @return {Promise<void>}
   *
   * @public
   * @async
   */
  async updateParticipantUsername(id, username) {
    await this._prisma.participant.update({
      where: { id },
      data: { username }
    });
  }

  /**
   * Update current win streak of a participant
   *
   * @param {number} id           Participant ID
   * @param {number} currentCombo New win streak
   *
   * @return {Promise<void>}
   *
   * @public
   * @async
   */
  async updateParticipantCurrentCombo(id, currentCombo) {
    await this._prisma.participant.update({
      where: { id },
      data: { currentCombo }
    });
  }

  /**
   * Updates current and maximal win streak of a participant
   *
   * @param {number} id           Participant ID
   * @param {number} currentCombo Current win streak
   * @param {number} maxCombo     Maximal win streak
   *
   * @return {Promise<void>}
   *
   * @public
   * @async
   */
  async updateParticipantAllCombos(id, currentCombo, maxCombo) {
    await this._prisma.participant.update({
      where: { id },
      data: { currentCombo, maxCombo }
    });
  }

  /**
   * Create a hit counter (counter of win streak) for participant
   *
   * @param {number} userId Participant ID
   * @param {number} chatId Chat ID
   * @param {number} year   Year when participant gets a hit (win)
   *
   * @return {Promise<void>}
   *
   * @public
   * @async
   */
  async createParticipantHitCounter(userId, chatId, year) {
    await this._prisma.hit.create({
      data: {
        userId,
        chatId,
        year,
        count: 1
      }
    });
  }

  /**
   * Create an empty hit counter (counter of win streak) for participant
   *
   * @param {number} userId Participant ID
   * @param {number} chatId Chat ID
   * @param {number} year   Year when participant gets a hit (win)
   *
   * @return {Promise<void>}
   *
   * @public
   * @async
   */
  async createEmptyParticipantHitCounter(userId, chatId, year) {
    await this._prisma.hit.create({
      data: {
        userId,
        chatId,
        year,
        count: 0
      }
    });
  }

  /**
   * Increment participant's hit counter
   *
   * @param {number} userId Participant ID
   * @param {number} year   Year when participant gets a hit (win)
   *
   * @return {Promise<void>}
   *
   * @public
   * @async
   */
  async incrementParticipantHitCounter(userId, year) {
    await this.increaseParticipantHitCounter(userId, year, 1);
  }

  /**
   * Add given amount of hits to participant's hit counter
   *
   * @param {number} userId Participant ID
   * @param {number} year   Year when participant gets hits (wins)
   * @param {number} amount Amount of hits
   *
   * @return {Promise<void>}
   *
   * @public
   * @async
   */
  async increaseParticipantHitCounter(userId, year, amount) {
    await this._prisma.hit.updateMany({
      where: { userId, year },
      data: {
        count: { increment: amount }
      }
    });
  }

  /**
   * Create table in database
   *
   * @param {string} table Table name
   *
   * @return {Promise<void>}
   *
   * @private
   * @async
   */
  async _createTable(table) {
    if (!Object.keys(schema).includes(table)) {
      throw new Error(`DDL for table "${table}" doesn't exist`);
    }

    await this._prisma.$executeRawUnsafe(schema[table]);
  }

  /**
   * Check if table exists in database
   *
   * @param {string} table Table name
   *
   * @return {Promise<boolean>}
   *
   * @private
   * @async
   */
  async _checkTableExist(table) {
    return await this._prisma.$queryRawUnsafe(`SELECT name FROM sqlite_master
        WHERE type = 'table' AND name = '${table}' LIMIT 1`)
      .then(result => result.length === 1);
  }
}

module.exports = DbManager;