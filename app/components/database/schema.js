/**
 * @module Schema
 * @desc Contains DDL for tables used in application
 *
 * @category Components
 * @subcategory Database
 */
module.exports = {
  chats: `CREATE TABLE chats (
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      tgId INTEGER NOT NULL,
      lastRaffleDate DATETIME NULL DEFAULT NULL,
      lastRaffleWinnerId INTEGER NULL DEFAULT NULL CHECK(lastRaffleWinnerId >= 0),
      lastGotyRaffleDate DATETIME NULL DEFAULT NULL,
      lastGotyRaffleWinnerId INTEGER NULL DEFAULT NULL CHECK(lastGotyRaffleWinnerId >= 0),
      earliestGotyRaffleDate DATETIME NOT NULL DEFAULT '2021-12-18 00:00:00'
    );
    create unique index chatId on chats (tgId);`,
  participants: `CREATE TABLE participants (
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      tgId INTEGER NOT NULL CHECK(tgId >= 0),
      username TEXT NOT NULL CHECK(LENGTH(username) <= 32),
      chatId INTEGER NOT NULL CHECK(chatId >= 0),
      isActive BOOLEAN NOT NULL DEFAULT 1,
      currentCombo INTEGER NOT NULL DEFAULT 0 CHECK(currentCombo >= 0),
      maxCombo INTEGER NOT NULL DEFAULT 0 CHECK(maxCombo >= 0),
      FOREIGN KEY (chatId) REFERENCES chats (id) ON UPDATE NO ACTION ON DELETE CASCADE
    );
    create index userId_chatId on participants (chatId, tgId, isActive);`,
  hits: `CREATE TABLE hits (
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      userId INTEGER NOT NULL CHECK(userId >= 0),
      chatId INTEGER NOT NULL CHECK(chatId >= 0),
      year INTEGER NOT NULL CHECK(year >= 0),
      count INTEGER NOT NULL CHECK(count >= 0),
      FOREIGN KEY (chatId) REFERENCES chats (id) ON UPDATE NO ACTION ON DELETE CASCADE,
      FOREIGN KEY (userId) REFERENCES participants (id) ON UPDATE NO ACTION ON DELETE CASCADE
    );
    create index userId on hits (userId);
    create index FK_hits_chats on hits (chatId);`
};