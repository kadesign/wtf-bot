const MicroLog = require('@kadesign/microlog').default;

const { sendMessage } = require('../utils/telegramApiHelper');

/**
 * Listeners manager.
 * Handles events on Telegraf bot
 *
 * @class
 *
 * @category Components
 */
class ListenersManager {
  /**
   * Creates ListenersManager instance
   *
   * @param {Telegraf}           telegrafBot    Reference to Telegraf bot instance (Telegraf.js library)
   * @param {ParticipantManager} participantMgr Reference to application's {@link ParticipantManager} instance
   */
  constructor(telegrafBot, participantMgr) {
    /**
     * Logger instance
     *
     * @type {MicroLog}
     *
     * @private
     */
    this._logger = new MicroLog(this);

    /**
     * Reference to Telegraf bot instance (Telegraf.js library)
     *
     * @type {Telegraf}
     *
     * @private
     */
    this._telegrafBot = telegrafBot;

    /**
     * Reference to application's {@link ParticipantManager} instance
     *
     * @type {ParticipantManager}
     *
     * @private
     */
    this._participantMgr = participantMgr;
  }

  /**
   * Registers event listeners on Telegraf bot
   *
   * @public
   */
  register() {
    this._telegrafBot.on('new_chat_members', async (ctx) => {
      const message = await this._participantMgr.checkIfReturned(ctx.message.chat.id, ctx.message.new_chat_member);
      if (message) sendMessage(ctx, message, this._logger);
    });
  }
}

module.exports = ListenersManager;